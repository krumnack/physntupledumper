#!/usr/bin/env python
#
# Copyright (C) 2002-2022  CERN for the benefit of the ATLAS collaboration
#
# @author Nils Krumnack


# Read the submission directory as a command line argument. You can
# extend the list of arguments with your private ones later on.
import argparse
parser = argparse.ArgumentParser()
parser.add_argument( '-d', '--data-type', dest = 'data_type',
                   action = 'store', type = str, default = 'data',
                   help = 'Type of data to run over. Valid options are data, mc, afii' )
parser.add_argument( '-s', '--submission-dir', dest = 'submission_dir',
                   action = 'store', type = str, default = 'submitDir',
                   help = 'Submission directory for EventLoop' )
parser.add_argument("--force-input", action = "store", dest = "force_input",
                  default = None,
                  help = "Force the given input file")
parser.add_argument( '-u', '--unit-test', dest='unit_test',
                   action = 'store_true', default = False,
                   help = 'Run the job in "unit test mode"' )
parser.add_argument( '--max-events', dest = 'max_events',
                   action = 'store', type = int, default = 500,
                   help = 'Number of events to run' )
parser.add_argument( '--algorithm-timer', dest='algorithm_timer',
                   action = 'store_true', default = False,
                   help = 'Run the job with a timer for each algorithm' )
parser.add_argument( '--for-compare', dest='for_compare',
                   action = 'store_true', default = False,
                   help = 'Configure the job for comparison of sequences vs blocks' )
parser.add_argument( '--physlite', dest='physlite',
                   action = 'store_true', default = False,
                   help = 'Configure the job for physlite' )
args = parser.parse_args()

# Set up (Py)ROOT.
import ROOT
ROOT.xAOD.Init().ignore()

# Force-load some xAOD dictionaries. To avoid issues from ROOT-10940.
ROOT.xAOD.TauJetContainer()

# ideally we'd run over all of them, but we don't have a mechanism to
# configure per-sample right now

dataType = args.data_type
forCompare = args.for_compare
isPhyslite = args.physlite

if dataType not in ["data", "mc", "afii"] :
    raise Exception ("invalid data type: " + dataType)

# Set up the sample handler object. See comments from the C++ macro
# for the details about these lines.
import os
sh = ROOT.SH.SampleHandler()
sh.setMetaString( 'nc_tree', 'CollectionTree' )
sample = ROOT.SH.SampleLocal (dataType)
if isPhyslite :
    inputfile = {"data": 'ASG_TEST_FILE_LITE_DATA',
                 "mc":   'ASG_TEST_FILE_LITE_MC',
                 "afii": 'ASG_TEST_FILE_LITE_MC_AFII'}
else :
    inputfile = {"data": 'ASG_TEST_FILE_DATA',
                 "mc":   'ASG_TEST_FILE_MC',
                 "afii": 'ASG_TEST_FILE_MC_AFII'}
if args.force_input :
    testFile = args.force_input
else:
    testFile = os.getenv (inputfile[dataType])
sample.add (testFile)
sh.add (sample)
sh.printContent()

from AthenaConfiguration.AllConfigFlags import initConfigFlags
flags = initConfigFlags()
flags.Input.Files = [testFile]
flags.lock()

# Create an EventLoop job.
job = ROOT.EL.Job()
job.sampleHandler( sh )
if args.max_events > 0:
    job.options().setDouble( ROOT.EL.Job.optMaxEvents, args.max_events )
if args.algorithm_timer :
    job.options().setBool( ROOT.EL.Job.optAlgorithmTimer, True )


from PhysNTupleDumper.NTupleDumper import makeSequence, printSequenceAlgs
algSeq = makeSequence (autoconfigFromFlags=flags)
printSequenceAlgs( algSeq ) # For debugging
algSeq.addSelfToJob( job )

# Make sure that both the ntuple and the xAOD dumper have a stream to write to.
job.outputAdd( ROOT.EL.OutputStream( 'ANALYSIS' ) )

# Find the right output directory:
submitDir = args.submission_dir
if args.unit_test:
    job.options().setString (ROOT.EL.Job.optSubmitDirMode, 'unique')
else :
    job.options().setString (ROOT.EL.Job.optSubmitDirMode, 'unique-link')


# Run the job using the direct driver.
driver = ROOT.EL.DirectDriver()

print ("submitting job now", flush=True)
driver.submit( job, submitDir )
