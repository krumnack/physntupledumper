
# Copyright (C) 2002-2023 CERN for the benefit of the ATLAS collaboration

# AnaAlgorithm import(s):
from AnalysisAlgorithmsConfig.ConfigBlock import ConfigBlock
from AnalysisAlgorithmsConfig.ConfigAccumulator import DataType
from AthenaConfiguration.Enums import LHCPeriod

class EventSelectionConfig (ConfigBlock):
    """the ConfigBlock for our custom event selection"""


    def __init__ (self) :
        super (EventSelectionConfig, self).__init__ ("EventSelection")
        self.addOption ('postfix', "", type=str)
        self.addOption ('jets', "", type=str)
        self.addOption ('electrons', "", type=str)
        self.addOption ('muons', "", type=str)
        self.addOption ('taus', "", type=str)


    def makeAlgs (self, config) :

        # IMPORTANT NOTE:  During configuration this function will be called
        # more than once, since some parts of the configuration can depend on
        # what is configured downstream, not just upstream.  For the most part
        # this is just fine as long as you stick to the standard patterns (i.e.
        # create components through the `config` object), but it means that
        # subsequent calls should perform the same configuration on each call.


        # the postfix is normally not used, but provided if we want to load the
        # same block multiple times (with different settings)
        postfix = self.postfix

        # add a new algorithm        
        alg = config.createAlgorithm( 'EventPreselectionAlg', 'EventSelectionAlg' + postfix )

        # add the input containers and their selections.  the actual container
        # names and selections will depend on whatever configuration has been
        # run before.  instead of configuring this manually in each block the
        # configuration accumulator tracks this and we can ask it for it.
        alg.jets, alg.jetsPreselection = config.readNameAndSelection (self.jets)
        alg.electrons, alg.electronsPreselection = config.readNameAndSelection (self.electrons)
        alg.muons, alg.muonsPreselection = config.readNameAndSelection (self.muons)
        alg.taus, alg.tausPreselection = config.readNameAndSelection (self.taus)

        # since we do one event selection per systematic we want to store the
        # selection for each (and filter on an OR of all sections).  that way we
        # can pick up the selection for each systematic in subsequent jobs.
        # whether this is useful or not will depend on the exact job run
        # afterwards (i.e. if the preselection is reapplied there is no need to
        # check this).
        alg.eventDecisionOutputDecoration = 'eventFilter_%SYS%'

        # one of the features of the configuration mechanism is that blocks can
        # pre-define the variables they have for the output n-tuple.  this has
        # some practical advantages in that the user doesn't have to track
        # manually what variables are available or how exactly they need to be
        # configured (e.g. whether they have systematics).
        config.addOutputVar ('EventInfo', alg.eventDecisionOutputDecoration,
                    'eventPreselect' + postfix)
