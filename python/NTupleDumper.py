# Copyright (C) 2002-2023 CERN for the benefit of the ATLAS collaboration
#
# @author Nils Krumnack

from AnaAlgorithm.AlgSequence import AlgSequence
from AsgAnalysisAlgorithms.AsgAnalysisAlgorithmsTest import pileupConfigFiles
from AnalysisAlgorithmsConfig.ConfigSequence import ConfigSequence
from AnalysisAlgorithmsConfig.ConfigAccumulator import ConfigAccumulator, DataType
from AnalysisAlgorithmsConfig.ConfigFactory import makeConfig
from PhysNTupleDumper.EventSelectionConfig import EventSelectionConfig

# Config:
triggerChainsPerYear = {
    '2015': ['HLT_e24_lhmedium_L1EM20VH || HLT_e60_lhmedium || HLT_e120_lhloose', 'HLT_mu20_iloose_L1MU15 || HLT_mu50', 'HLT_2g20_tight'],
    '2016': ['HLT_e26_lhtight_nod0_ivarloose || HLT_e60_lhmedium_nod0 || HLT_e140_lhloose_nod0', 'HLT_mu26_ivarmedium || HLT_mu50', 'HLT_g35_loose_g25_loose'],
    '2017': ['HLT_e26_lhtight_nod0_ivarloose || HLT_e60_lhmedium_nod0 || HLT_e140_lhloose_nod0', 'HLT_2g22_tight_L12EM15VHI', 'HLT_mu50'],
    '2018': ['HLT_e26_lhtight_nod0_ivarloose || HLT_e60_lhmedium_nod0 || HLT_e140_lhloose_nod0', 'HLT_g35_medium_g25_medium_L12EM20VH', 'HLT_mu26_ivarmedium', 'HLT_2mu14'],
    '2022': ['HLT_e26_lhtight_ivarloose_L1EM22VHI || HLT_e60_lhmedium_L1EM22VHI || HLT_e140_lhloose_L1EM22VHI'],
}

electronMinPt = 27e3
electronMaxEta = None
photonMinPt = 27e3
photonMaxEta = None
muonMinPt = 27e3
muonMaxEta = None
tauMinPt = 27e3
tauMaxEta = None
jetMinPt = 45e3
jetMaxEta = None


def makeSequence (*, dataType = None, isPhyslite = None, geometry = None,
            autoconfigFromFlags = None) :

    algSeq = AlgSequence()
    vars = []
    metVars = []

    # There are two ways to configure what kind of data we run on:  We can read
    # the configuration flags (as done in Athena), or manually pass in the type.
    # The main reason to do the later is that the EventLoop PrunDriver currently
    # doesn't work with the former.  This code snippet relies on the
    # ConfigAccumulator (which can handle either) and then retrieves the
    # information we need for the subsequent configuration.
    configAccumulator = ConfigAccumulator (algSeq, dataType=dataType, isPhyslite=isPhyslite,
            geometry=geometry, autoconfigFromFlags=autoconfigFromFlags)
    dataType = configAccumulator.dataType()
    isPhyslite = configAccumulator.isPhyslite()


    # there are two ways to configure blocks that you may see around:
    #   configSeq += makeConfig (...)
    #   configSeq.setOptionValue (".option", value)
    # or
    #   subConfig = makeConfig (...)
    #   subConfig.setOptionValue (".option", value)
    #   configSeq += subConfig
    # These days I lean more towards the later, as it makes some things a little
    # more robust (even if it is slightly more verbose).

    # make a configuration sequence to which we can then add the individual
    # configuration blocks.
    configSeq = ConfigSequence ()

    # add the common services needed in all the jobs
    subConfig = makeConfig ('CommonServices', None)
    configSeq += subConfig

    # FIXME: this should probably be run on PHYSLITE, but the test fails with:
    #   overran integrated luminosity for RunNumber=363262 (0.000000 vs 0.000000)
    if not isPhyslite :
        campaign, files, prwfiles, lumicalcfiles = None, None, None, None
        useDefaultConfig = False
        if autoconfigFromFlags is not None:
            campaign = autoconfigFromFlags.Input.MCCampaign
            files = autoconfigFromFlags.Input.Files
            useDefaultConfig = True
        else:
            # need to allow for conversion of dataType from string to enum for the call to pileupConfigFiles
            prwfiles, lumicalcfiles = pileupConfigFiles( {'data': DataType.Data, 'mc': DataType.FullSim, 'afii': DataType.FastSim}.get(dataType, dataType) )

        subConfig = makeConfig ('Event.PileupReweighting', None)
        subConfig.setOptionValue ('.campaign', campaign, noneAction='ignore')
        subConfig.setOptionValue ('.files', files, noneAction='ignore')
        subConfig.setOptionValue ('.useDefaultConfig', useDefaultConfig)
        subConfig.setOptionValue ('.userPileupConfigs', prwfiles, noneAction='ignore')
        subConfig.setOptionValue ('.userLumicalcFiles', lumicalcfiles, noneAction='ignore')
        configSeq += subConfig

    else :
        # ideally the above block would work both for PHYS and PHYSLITE, but
        # since we disabled it we need to add these variables manually.
        vars += [ 'EventInfo.runNumber     -> runNumber',
                  'EventInfo.eventNumber   -> eventNumber',
                  'EventInfo.mcChannelNumber -> mcChannelNumber']


    # Skip events with no primary vertex, and perform loose jet cleaning
    subConfig = makeConfig ('Event.Cleaning', None)
    subConfig.setOptionValue ('.runEventCleaning', True)
    configSeq += subConfig

    # set up the jet analysis config
    subConfig = makeConfig( 'Jets', 'AnaJets', jetCollection='AntiKt4EMPFlowJets')
    subConfig.setOptionValue ('.runJvtUpdate', False )
    subConfig.setOptionValue ('.runNNJvtUpdate', True )
    configSeq += subConfig

    # set up b-tagging config
    btagger = "DL1dv01"
    btagWP = "FixedCutBEff_60"
    subConfig = makeConfig( 'FlavourTagging', 'AnaJets.ftag' )
    subConfig.setOptionValue ('.btagger', btagger)
    subConfig.setOptionValue ('.btagWP', btagWP)
    subConfig.setOptionValue ('.kinematicSelection', True )
    configSeq += subConfig

    # set up the jet-jvt config
    # TO DO: check whether this is indeed needed.  It ought to be included in
    # the main jet config block already.
    subConfig = makeConfig( 'Jets.Jvt', 'AnaJets' )
    configSeq += subConfig

    # set up the electron analysis config
    likelihood = True
    recomputeLikelihood=False
    subConfig = makeConfig ('Electrons', 'AnaElectrons')
    configSeq += subConfig
    subConfig = makeConfig ('Electrons.Selection', 'AnaElectrons.loose')
    if likelihood:
        subConfig.setOptionValue ('.likelihoodWP', 'LooseBLayerLH')
    else:
        subConfig.setOptionValue ('.likelihoodWP', 'LooseDNN')
    # FIXME: fails for PHYSLITE with missing data item
    # ptvarcone30_Nonprompt_All_MaxWeightTTVALooseCone_pt1000
    if not isPhyslite :
        subConfig.setOptionValue ('.isolationWP', 'Loose_VarRad')
    else :
        subConfig.setOptionValue ('.isolationWP', 'NonIso')
    subConfig.setOptionValue ('.recomputeLikelihood', recomputeLikelihood)
    configSeq += subConfig

    # set up the photon analysis config
    subConfig = makeConfig ('Photons', 'AnaPhotons')
    subConfig.setOptionValue ('.recomputeIsEM', False)
    configSeq += subConfig
    subConfig = makeConfig ('Photons.Selection', 'AnaPhotons.tight')
    subConfig.setOptionValue ('.qualityWP', 'Tight')
    subConfig.setOptionValue ('.isolationWP', 'FixedCutTight')
    subConfig.setOptionValue ('.recomputeIsEM', False)
    configSeq += subConfig


    # set up the muon analysis config
    subConfig = makeConfig ('Muons', 'AnaMuons')
    configSeq += subConfig
    subConfig = makeConfig ('Muons.Selection', 'AnaMuons.medium')
    subConfig.setOptionValue ('.quality', 'Medium')
    subConfig.setOptionValue ('.isolation', 'Loose_VarRad')
    configSeq += subConfig


    # set up the tau analysis config
    subConfig = makeConfig ('TauJets', 'AnaTauJets')
    configSeq += subConfig
    subConfig = makeConfig ('TauJets.Selection', 'AnaTauJets.tight')
    subConfig.setOptionValue ('.quality', 'Tight')
    configSeq += subConfig


    # TO DO: not running on data should probably be absorbed into the event
    # generator config, not required to do manually here.
    if dataType is not DataType.Data :
        # set up the generator analysis sequence
        subConfig = makeConfig( 'Event.Generator', None)
        subConfig.setOptionValue ('.saveCutBookkeepers', True)
        subConfig.setOptionValue ('.runNumber', 284500)
        subConfig.setOptionValue ('.cutBookkeepersSystematics', True)
        configSeq += subConfig


    # this adds a pt-eta selections to the objects.  in the future we'll likely
    # add that as an option directly to the object configuration blocks.
    subConfig = makeConfig ('Selection.PtEta', 'AnaElectrons')
    subConfig.setOptionValue ('.selectionDecoration', 'selectPtEta')
    subConfig.setOptionValue ('.minPt', electronMinPt, noneAction='ignore')
    subConfig.setOptionValue ('.maxEta', electronMaxEta, noneAction='ignore')
    configSeq += subConfig
    subConfig = makeConfig ('Selection.PtEta', 'AnaPhotons')
    subConfig.setOptionValue ('.selectionDecoration', 'selectPtEta')
    subConfig.setOptionValue ('.minPt', photonMinPt, noneAction='ignore')
    subConfig.setOptionValue ('.maxEta', photonMaxEta, noneAction='ignore')
    configSeq += subConfig
    subConfig = makeConfig ('Selection.PtEta', 'AnaMuons')
    subConfig.setOptionValue ('.selectionDecoration', 'selectPtEta')
    subConfig.setOptionValue ('.minPt', muonMinPt, noneAction='ignore')
    subConfig.setOptionValue ('.maxEta', muonMaxEta, noneAction='ignore')
    configSeq += subConfig
    subConfig = makeConfig ('Selection.PtEta', 'AnaTauJets')
    subConfig.setOptionValue ('.selectionDecoration', 'selectPtEta')
    subConfig.setOptionValue ('.minPt', tauMinPt, noneAction='ignore')
    subConfig.setOptionValue ('.maxEta', tauMaxEta, noneAction='ignore')
    configSeq += subConfig
    subConfig = makeConfig ('Selection.PtEta', 'AnaJets')
    subConfig.setOptionValue ('.selectionDecoration', 'selectPtEta')
    subConfig.setOptionValue ('.minPt', jetMinPt, noneAction='ignore')
    subConfig.setOptionValue ('.maxEta', jetMaxEta, noneAction='ignore')
    configSeq += subConfig

    # produce object cut flows.  these are histograms that show how many object
    # candidates survive each quality/cleaning cut.
    subConfig = makeConfig ('Selection.ObjectCutFlow', 'AnaElectrons.loose')
    configSeq += subConfig
    subConfig = makeConfig ('Selection.ObjectCutFlow', 'AnaPhotons.tight')
    configSeq += subConfig
    subConfig = makeConfig ('Selection.ObjectCutFlow', 'AnaMuons.medium')
    configSeq += subConfig
    subConfig = makeConfig ('Selection.ObjectCutFlow', 'AnaTauJets.tight')
    configSeq += subConfig
    subConfig = makeConfig ('Selection.ObjectCutFlow', 'AnaJets.jvt')
    configSeq += subConfig

    # set up the met analysis algorithm config
    subConfig = makeConfig ('MissingET', 'AnaMET')
    subConfig.setOptionValue ('.jets', 'AnaJets')
    subConfig.setOptionValue ('.taus', 'AnaTauJets.tight')
    subConfig.setOptionValue ('.electrons', 'AnaElectrons.loose')
    subConfig.setOptionValue ('.photons', 'AnaPhotons.tight')
    subConfig.setOptionValue ('.muons', 'AnaMuons.medium')
    configSeq += subConfig


    # set up the overlap analysis algorithm config
    subConfig = makeConfig( 'OverlapRemoval', None )
    subConfig.setOptionValue ('.electrons',   'AnaElectrons.loose')
    subConfig.setOptionValue ('.photons',     'AnaPhotons.tight')
    subConfig.setOptionValue ('.muons',       'AnaMuons.medium')
    subConfig.setOptionValue ('.jets',        'AnaJets')
    subConfig.setOptionValue ('.taus',        'AnaTauJets.tight')
    subConfig.setOptionValue ('.inputLabel',  'preselectOR')
    subConfig.setOptionValue ('.outputLabel', 'passesOR' )

    # ask to be added to the baseline selection for all objects, and to
    # provide a preselection for the objects in subsequent algorithms
    subConfig.setOptionValue ('.selectionName', '')
    subConfig.setOptionValue ('.addPreselection', True)
    configSeq += subConfig


    # set up the trigger analysis sequence:
    subConfig = makeConfig( 'Trigger.Chains', None )
    subConfig.setOptionValue ('.triggerChainsPerYear', triggerChainsPerYear )
    subConfig.setOptionValue ('.noFilter', True )
    subConfig.setOptionValue ('.electronID', 'Tight' )
    subConfig.setOptionValue ('.electronIsol', 'Tight_VarRad')
    subConfig.setOptionValue ('.photonIsol', 'TightCaloOnly')
    subConfig.setOptionValue ('.muonID', 'Tight')
    subConfig.setOptionValue ('.electrons', 'AnaElectrons' )
    subConfig.setOptionValue ('.photons', 'AnaPhotons' )
    subConfig.setOptionValue ('.muons', 'AnaMuons' )
    configSeq += subConfig


    # apply our custom event selection configuration (using our custom algorithm)
    subConfig = ConfigSequence ()
    subConfig.append (EventSelectionConfig ())
    subConfig.setOptionValue ('.electrons',   'AnaElectrons.loose')
    subConfig.setOptionValue ('.muons',       'AnaMuons.medium')
    subConfig.setOptionValue ('.jets',        'AnaJets')
    subConfig.setOptionValue ('.taus',        'AnaTauJets.tight')
    configSeq += subConfig


    # The prefixes for the output containers.  In the future this will likely be
    # set automatically based on the configured objects.
    outputContainers = {'mu_' : 'OutMuons',
                        'el_' : 'OutElectrons',
                        'ph_' : 'OutPhotons',
                        'tau_': 'OutTauJets',
                        'jet_': 'OutJets',
                        'met_': 'AnaMET',
                        ''    : 'EventInfo'}

    # perform the object thinning.  this removes objects that fail the selection
    # for **all** systematics.  this guarantees that the same objects are
    # available for all systematics,  allowing to share variables that are not
    # affected by systematics (e.g. eta and phi are often not affected by
    # systematics).  In the future this object thinning will likely be absorbed
    # into the overall output block, so users no longer would have to specify it
    # indiviually.
    subConfig = makeConfig ('Output.Thinning', 'AnaElectrons.Thinning')
    subConfig.setOptionValue ('.selectionName', 'loose')
    subConfig.setOptionValue ('.outputName', 'OutElectrons')
    configSeq += subConfig
    subConfig = makeConfig ('Output.Thinning', 'AnaPhotons.Thinning')
    subConfig.setOptionValue ('.selectionName', 'tight')
    subConfig.setOptionValue ('.outputName', 'OutPhotons')
    configSeq += subConfig
    subConfig = makeConfig ('Output.Thinning', 'AnaMuons.Thinning')
    subConfig.setOptionValue ('.selectionName', 'medium')
    subConfig.setOptionValue ('.outputName', 'OutMuons')
    configSeq += subConfig
    subConfig = makeConfig ('Output.Thinning', 'AnaTauJets.Thinning')
    subConfig.setOptionValue ('.selectionName', 'tight')
    subConfig.setOptionValue ('.outputName', 'OutTauJets')
    configSeq += subConfig
    subConfig = makeConfig ('Output.Thinning', 'AnaJets.Thinning')
    subConfig.setOptionValue ('.outputName', 'OutJets')
    configSeq += subConfig

    # set up the main output configuration.  for the most part this needs very
    # little configuration, because the individual configurations will already
    # have notified the output subsystem which variables need to be added to the
    # n-tuple.
    subConfig = makeConfig ('Output.Simple', 'Output')
    subConfig.setOptionValue ('.treeName', 'analysis')
    subConfig.setOptionValue ('.vars', vars)
    subConfig.setOptionValue ('.metVars', metVars)
    subConfig.setOptionValue ('.containers', outputContainers)
    configSeq += subConfig

    # configure the actual algorithm sequence.  up to now we have just set
    # configuration options, this now translates it.
    configSeq.fullConfigure (configAccumulator)

    from AnaAlgorithm.DualUseConfig import isAthena, useComponentAccumulator
    if isAthena and useComponentAccumulator:
        return configAccumulator.CA
    else:
        return algSeq



def printSequenceAlgs (sequence) :
    """print the algorithms in the sequence without the sequence structure

    This is mostly meant for easy comparison of different sequences
    during configuration, particularly the sequences resulting from
    the old sequence configuration and the new block configuration.
    Those have different sequence structures in the output, but the
    algorithms should essentially be configured the same way."""
    if isinstance (sequence, AlgSequence) :
        for alg in sequence :
            printSequenceAlgs (alg)
    else :
        # assume this is an algorithm then
        print (sequence)
