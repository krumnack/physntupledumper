/*
  Copyright (C) 2002-2022 CERN for the benefit of the ATLAS collaboration
*/

/// @author Nils Krumnack



//
// includes
//

#include <PhysNTupleDumper/EventPreselectionAlg.h>

#include <SystematicsHandles/SysFilterReporter.h>
#include <SystematicsHandles/SysFilterReporterCombiner.h>

//
// method implementations
//

EventPreselectionAlg ::
EventPreselectionAlg (const std::string& name, 
                      ISvcLocator* pSvcLocator)
  : AnaAlgorithm (name, pSvcLocator)
{
}



StatusCode EventPreselectionAlg ::
initialize ()
{
  ANA_CHECK (m_filterParams.initialize (m_systematicsList));
  ANA_CHECK (m_jetsHandle.initialize (m_systematicsList));
  ANA_CHECK (m_jetsPreselection.initialize (m_systematicsList, m_jetsHandle, SG::AllowEmpty));
  ANA_CHECK (m_electronsHandle.initialize (m_systematicsList));
  ANA_CHECK (m_electronsPreselection.initialize (m_systematicsList, m_electronsHandle, SG::AllowEmpty));
  ANA_CHECK (m_muonsHandle.initialize (m_systematicsList));
  ANA_CHECK (m_muonsPreselection.initialize (m_systematicsList, m_muonsHandle, SG::AllowEmpty));
  ANA_CHECK (m_tausHandle.initialize (m_systematicsList));
  ANA_CHECK (m_tausPreselection.initialize (m_systematicsList, m_tausHandle, SG::AllowEmpty));
  ANA_CHECK (m_systematicsList.initialize());
  return StatusCode::SUCCESS;
}



StatusCode EventPreselectionAlg ::
execute ()
{
  CP::SysFilterReporterCombiner filterCombiner (m_filterParams, false);

  for (const auto& sys : m_systematicsList.systematicsVector())
  {
    CP::SysFilterReporter filter (filterCombiner, sys);

    unsigned numJets = 0u;
    const xAOD::JetContainer *jets = nullptr;
    ANA_CHECK (m_jetsHandle.retrieve (jets, sys));
    for (const xAOD::Jet *jet : *jets)
    {
      if (m_jetsPreselection.getBool (*jet, sys))
        numJets += 1u;
    }
    // we want at least two jets
    if (numJets < 2u)
      continue;

    unsigned numPos = 0u, numNeg = 0u;

    const xAOD::ElectronContainer *electrons = nullptr;
    ANA_CHECK (m_electronsHandle.retrieve (electrons, sys));
    for (const xAOD::Electron *electron : *electrons)
    {
      if (m_electronsPreselection.getBool (*electron, sys))
      {
        if (electron->charge() > 0)
          numPos += 1u;
        else if (electron->charge() < 0)
          numNeg += 1u;
      }
    }

    const xAOD::MuonContainer *muons = nullptr;
    ANA_CHECK (m_muonsHandle.retrieve (muons, sys));
    for (const xAOD::Muon *muon : *muons)
    {
      if (m_muonsPreselection.getBool (*muon, sys))
      {
        if (muon->charge() > 0)
          numPos += 1u;
        else if (muon->charge() < 0)
          numNeg += 1u;
      }
    }

    const xAOD::TauJetContainer *taus = nullptr;
    ANA_CHECK (m_tausHandle.retrieve (taus, sys));
    for (const xAOD::TauJet *tau : *taus)
    {
      if (m_tausPreselection.getBool (*tau, sys))
      {
        if (tau->charge() > 0)
          numPos += 1u;
        else if (tau->charge() < 0)
          numNeg += 1u;
      }
    }

    // we want at least two opposite charge leptons
    if (numPos == 0u || numNeg == 0u)
      continue;

    filter.setPassed ();
  }

  return StatusCode::SUCCESS;
}



StatusCode EventPreselectionAlg ::
finalize ()
{
  ANA_CHECK (m_filterParams.finalize ());
  return StatusCode::SUCCESS;
}
