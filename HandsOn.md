# PHYS-PHYSLITE Hands-On

## Setup Source Area

If you have a source area setup for use with ATLAS cmake and ATLAS
releases (e.g. like done in the software tutorial).  If you don't have
that, follow these instructions:

    mkdir tutorial
    cd tutorial
    mkdir source
    mkdir build
    mkdir run

Inside the source area you need a generic top-level ATLAS
`CMakeLists.txt`, e.g. as found here:
https://atlassoftwaredocs.web.cern.ch/ABtutorial/release_setup/

## Checkout And Build N-Tuple Code

You will need to checkout and compile the n-tuple code:

    cd source
    git clone https://:@gitlab.cern.ch:8443/krumnack/PhysNTupleDumper.git
    cd ../build
    asetup AnalysisBase,24.2.29
    cmake ../source
    make
    source x86*/setup.sh

Note that the n-tuple package mostly contains python code, but the compilation
is still needed to put the python files in the right locations, etc.  It (at the
moment) also contains an algorithm for event selection, but in the future it may
rely on standard event selection algorithms in the release.

You can also use AthAnalysis instead, modify the commands below
accordingly.

## Run The N-Tuple Code

Let's run the n-tuple code twice, once on the PHYS test-file, once on
the PHYSLITE test-file.  Both of these are distributed via cvmfs and
defined in the release:

    PhysNTupleDumper_eljob.py --max-events 500 --force-input $ASG_TEST_FILE_MC
    PhysNTupleDumper_eljob.py --max-events 500 --force-input $ASG_TEST_FILE_LITE_MC

This will create two submissions directories, with content that should
essentially be identical.

If you chose to run in Athena instead the job options file is called
PhysNTupleDumper_jobOptions.py.

## Look At The Code

Now let's take a look at the code:  https://gitlab.cern.ch:8443/krumnack/physntupledumper/-/blob/master/python/NTupleDumper.py

This is a fairly complete n-tuple dumping code based on the CP algorithms.  You
can go through it, noting the different blocks we configure for different
objects/tasks.  There are still some things we are trying to change/optimize about this configuration.