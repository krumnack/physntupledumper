/*
  Copyright (C) 2002-2022 CERN for the benefit of the ATLAS collaboration
*/

/// @author Nils Krumnack



#ifndef PHYS_NTUPLE_DUMPER_EVENT_PRESELECTION_ALG_H
#define PHYS_NTUPLE_DUMPER_EVENT_PRESELECTION_ALG_H

#include <AnaAlgorithm/AnaAlgorithm.h>
#include <SelectionHelpers/SysReadSelectionHandle.h>
#include <SystematicsHandles/SysListHandle.h>
#include <SystematicsHandles/SysReadHandle.h>
#include <SystematicsHandles/SysFilterReporterParams.h>
#include <xAODJet/JetContainer.h>
#include <xAODEgamma/ElectronContainer.h>
#include <xAODMuon/MuonContainer.h>
#include <xAODTau/TauJetContainer.h>

/// \brief an algorithm for performing our event preselection
///
/// This algorithm uses \ref SysFilterReporter to record the general
/// event preselection for our analysis.  The main purpose of this
/// algorithm is to cut down on the size of our output n-tuple,
/// particularly when running on data and background monte carlo.

class EventPreselectionAlg final : public EL::AnaAlgorithm
{
public:

  /// \brief the standard constructor
  EventPreselectionAlg (const std::string& name, 
                        ISvcLocator* pSvcLocator);


  /// \brief overridden methods from the algorithm base class
  StatusCode initialize () override;
  StatusCode execute () override;
  StatusCode finalize () override;


private:

  /// \brief the systematics list we run
  CP::SysListHandle m_systematicsList {this};

  /// \brief the event filter reporter
  CP::SysFilterReporterParams m_filterParams {this, "analysis event preselection"};

  /// \brief the jet container we run on
  CP::SysReadHandle<xAOD::JetContainer> m_jetsHandle {
    this, "jets", "", "the jet collection to run on"};

  /// \brief the preselection we apply to our jets
  CP::SysReadSelectionHandle m_jetsPreselection {
    this, "jetsPreselection", "", "the jet preselection to apply"};

  /// \brief the electron container we run on
  CP::SysReadHandle<xAOD::ElectronContainer> m_electronsHandle {
    this, "electrons", "", "the electron collection to run on"};

  /// \brief the preselection we apply to our electrons
  CP::SysReadSelectionHandle m_electronsPreselection {
    this, "electronsPreselection", "", "the electron preselection to apply"};

  /// \brief the muon container we run on
  CP::SysReadHandle<xAOD::MuonContainer> m_muonsHandle {
    this, "muons", "", "the muon collection to run on"};

  /// \brief the preselection we apply to our muons
  CP::SysReadSelectionHandle m_muonsPreselection {
    this, "muonsPreselection", "", "the muon preselection to apply"};

  /// \brief the tau container we run on
  CP::SysReadHandle<xAOD::TauJetContainer> m_tausHandle {
    this, "taus", "", "the tau collection to run on"};

  /// \brief the preselection we apply to our taus
  CP::SysReadSelectionHandle m_tausPreselection {
    this, "tausPreselection", "", "the tau preselection to apply"};
};

#endif
